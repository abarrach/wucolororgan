#pragma once

#include "ofMain.h"
#include "ofxCv.h"
#include "wuImagePlus.h"
#include "ofxOsc.h"

#define MAX_TYPES 3

#define SHOW_COLOR 0
#define SHOW_CALIBRATE 1
#define NUM_SHOWS 2 // estic passant de mostear el sync

struct ColorSpot {
    string  mName;
    int     mDegree;
    ofColor color;
    int     threshold;
};

struct BlobInfoSt
{
    BlobInfoSt()
    : isActive(false)
    , id(0)
    , gruix(0)
    , gr_medi(0)
    , type(0)
    , roughness(0)
    , tightness(1)
    , area(0)
    , area_per(0)
    , posX(0)
    , rad(0){}
    
	bool isActive;
    int id;
	ofPoint min_pt;
	ofPoint max_pt;
	int gruix;  // gruix per on passa l' scan
	int gr_medi; // gruix mig del blob
	ofColor color;
	int type;
	int colortype;
	int roughness; // puntiagut o suau-arrodonit
	float tightness; // (inf-1)->estret horitzontal, (1) quadrat, (1-0) estret vertical
	int area;
	float area_per; // % d' area ocupada respecte l' area del rectangle
	int length;
	float posX;
    float rad;
};

enum {
    STAT_NONE,
    STAT_CENTER_CLICKED,
    STAT_LABEL_CLICKED,
    STAT_READY
};


class testApp : public ofBaseApp {
public:
	void setup();
	void update();
	void draw();
	void mousePressed(int x, int y, int button);
	void keyPressed(int key);

    void get_blob_info(vector<cv::Point> * pts, cv::Rect boundingRect, int area, BlobInfoSt * info);
	void isActive_blob_info(vector<cv::Point> * pts, cv::Rect boundingRect, BlobInfoSt * info);
    void sendOsc(BlobInfoSt &info);

    bool loadSettings(const string &inFilename);
    void saveSettings(const string &inFilename);

    // camera
	ofVideoGrabber mInput;	
	int						mCaptureWidth, mCaptureHeight;

	ofxCv::ContourFinder contourFinder;
	ofxCv::ContourFinder contourFinderShow;
	float threshold;
	ofxCv::TrackingColorMode trackingColorMode;
	ofColor targetColor;
    wuImagePlus tocataImg;
    ofImage colorImg;
    
    // Tocata position variables
	ofPoint vinylCenter;
	int vinylRad, labelRad, numSynths;
	int synth_count[MAX_TYPES], synth_count_old[MAX_TYPES];

    // color && info
    vector<ColorSpot>           mColorData;
	vector <BlobInfoSt>     blobInfo;

    // Config / OSC stuff
    bool bSendToSC;
    ofxOscSender                mSender;
    string                      mSenderAddr;
    int                         mSenderPort;
    int                         mCameraId;
    int                         mMinArea;
    int                         mMaxArea;
    int                         mHighOctaveThresh;
    int                         mLowOctaveThresh;

    int  vShowImage, mStatus;
    
    ofTrueTypeFont              mFont;
    ofTrueTypeFont              mFontSmall;
    

};
