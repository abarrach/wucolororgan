#include "testApp.h"

using namespace ofxCv;
using namespace cv;

std::string trim(const std::string& str,
                 const std::string& whitespace = " \t")
{
    const size_t strBegin = str.find_first_not_of(whitespace);
    if (strBegin == std::string::npos)
        return ""; // no content
    
    const size_t strEnd = str.find_last_not_of(whitespace);
    const size_t strRange = strEnd - strBegin + 1;
    
    return str.substr(strBegin, strRange);
}

// sort BlobInfo by Y position
bool sort_fun (const BlobInfoSt& x,const BlobInfoSt& y) {
    return x.gruix > y.gruix;
}


bool testApp::loadSettings(const string &inFilename)
{
    bool bSuccess = false;
    
    int tmp;
    
    ofFile file(inFilename);
    if (file.exists()) {
        stringstream ss;
        
        enum SettingLoaders {
            SENDER = 0,
            CAMERA,
            MIN_AREA,
            MAX_AREA,
            THRESHOLD,
            LOW_OCT,
            WHITE_BALANCE,
            WORKSPACE_CORNERS
        };
        
        int whichSetting = 0;
        for( string line; getline(file, line) && !bSuccess; ) {
            line = trim(line);
            if ( !line.empty() && line[0] != '#' ) {
                ss.str(line);
                ss.clear();
                switch (whichSetting++) {
                    case SENDER: {
                        ss >> mSenderAddr >> mSenderPort;
                    } break;
                    case CAMERA: {
                        ss >> mCameraId;
                    } break;
                    case MIN_AREA: {
                        ss >> mMinArea;
                    } break;
                    case MAX_AREA: {
                        ss >> mMaxArea;
                    } break;
                    case THRESHOLD: {
                        ss >> threshold;
                    } break;
                    case LOW_OCT: {
                        ss >> mLowOctaveThresh;
                    } break;
                    case WHITE_BALANCE: {
                        ss >> tmp >> tmp >> tmp;
                    } break;
                    case WORKSPACE_CORNERS: {
                        for (size_t i = 0; i < 4; i++) {
                            ss >> tmp >> tmp;
                        }
                        bSuccess = true;
                    } break;
                    default:
                        ofLog() << "Case " << whichSetting << " not handled!";
                        break;
                }
            }
        }
        // Now load the color database
        enum ColorLoaders {
            NAME = 0,
            DEGREE,
            HUE,
//            SAT,
            BRI
        };
        int nColorLoadpoint = 0;
        ofPtr<ColorSpot> pCurColor(new ColorSpot());
        for( string line; getline(file, line); ) {
            line = trim(line);
            if ( !line.empty() && line[0] != '#' ) {
                double mean, var, range;
                ss.str(line);
                ss.clear();
                switch (nColorLoadpoint++) {
                    case NAME:
                        ss >> pCurColor->mName;
                        break;
                    case DEGREE:
                        ss >> pCurColor->mDegree;
                        break;
                    case HUE:
                        ss >> mean >> var >> range;
//                        cout << "(---" << mean << "---)";
                        pCurColor->color.r = mean;
                        pCurColor->color.g = var;
                        pCurColor->color.b = range;
                        break;
//                    case SAT:
//                        ss >> pCurColor->threshold;
//                        break;
                    case BRI:
                        ss >> pCurColor->threshold;
                        cout << "Loading a color: " << pCurColor->mName << " Degree: " << pCurColor->mDegree << "(---" << (int)pCurColor->color.r << " " << (int)pCurColor->color.g << " " << (int)pCurColor->color.b << "---)" << endl;
                        mColorData.push_back(*pCurColor);
                        pCurColor = ofPtr<ColorSpot>(new ColorSpot());
                        nColorLoadpoint = 0;  // Now start another color
                        break;
                    default:
                        ofLogWarning() << "Shouldn't have gotten here!";
                        break;
                }
            }
        }
#ifdef USE_BRIGHTNESS_FEATURE
        ofLog() << "Including Brightness as a feature";
#else
        ofLog() << "Not including Brightness as a feature";
#endif
        
    }
    else {
        ofLogError() << "File " << inFilename << " not found.";
    }
    
    return bSuccess;
}

void testApp::saveSettings(const string &inFilename)
{
    ofFile file(inFilename, ofFile::WriteOnly);
    file << "# sender_addr server_port" << endl;
    file << mSenderAddr << " " << mSenderPort << endl;
    file << endl;
    file << "# camera device number (0-N) -- index of the webcam to use, normally 0 or 1" << endl;
    file << mCameraId << endl;
    file << endl;
    file << "# minimum blob area in pixels" << endl;
    file << mMinArea << endl;
    file << endl;
    file << "# maximum blob area in pixels" << endl;
    file << mMaxArea << endl;
    file << endl;
    file << "# threshold" << endl;
    file << threshold << endl;
    file << endl;
    file << "# above this area (in pixels), we shift down an octave" << endl;
    file << mLowOctaveThresh << endl;
    file << endl;
    file << "# white balance color scaling (Red Green Blue)" << endl;
    file << 1 << " " << 1 << " " << 1 << endl;
    file << endl;
    file << "# workspace corners (4 XY coords --> top left, right, bottom right, left -- i.e. clockwise from upper left)" << endl;
    for (size_t i = 0; i < 4; i++) {
        file << "\t\t" << 1 << " " << 1;
    }
    file << endl << endl;
    
    file << "# Stats for colors, in the following format:" << endl;
    file << "#   colorName" << endl;
    file << "#   noteDegree (in scale: 0 = root, 7 = up one octave)" << endl;
    file << "#   hueMean hueVariance (hue is -180 to 180 or 0-360 degrees)" << endl;
    file << "#   saturationMean saturationVariance (sat is 0-255)" << endl;
    file << "#   brightnessMean brightnessVariance (bri is 0-255)" << endl;
    file << endl;
    
    for (size_t i = 0; i < mColorData.size(); i++) {
        ColorSpot &curColor = mColorData[i];
        file << curColor.mName << endl;
        file << curColor.mDegree << endl;
        file << (int)curColor.color.r << " " << (int)curColor.color.g << " " << (int)curColor.color.b << endl;
        file << curColor.threshold << endl;
        file << endl;
    }
    
    ofLog() << "Saved settings file: '" << inFilename << "'";
}

void testApp::setup() {

    mSenderAddr = "127.0.0.1";   // Needs to be set to the server's address (using config file)
    mSenderPort = 12345;            // Default SuperCollider language port
    mCameraId = 1;                  // Which camera index to use (normally 1, or 0 if you have no built-in webcam)
    mMinArea = 10 * 10;             // we ignore blobs below this size in pixels
    mMaxArea = 50 * 50;             // we ignore blobs above this size in pixels
    mHighOctaveThresh = 400;        // below this area (in pixels), we shift up an octave
    mLowOctaveThresh = 1200;        // above this area (in pixels), we shift down an octave
    
	mCaptureWidth = 640;
	mCaptureHeight = 480;
    
    // Load configuration file
    if (!loadSettings("wuTocataSetup.txt")) {
        ofLogWarning() << "Problem with setup file - OSC may not be correctly configured.";
    }
    
 	ofBackground(0, 0, 0);
//	ofSetFrameRate(20);
	ofSetVerticalSync(false);

    mStatus = STAT_NONE;
    vShowImage = SHOW_COLOR;
    
    mInput.listDevices();
    
    mInput.setDeviceID(mCameraId);
	mInput.initGrabber(mCaptureWidth, mCaptureHeight);
	mInput.setDesiredFrameRate(60);
    
   
    // Setup OSC communications
    mSenderAddr = "127.0.0.1";   // Needs to be set to the server's address (using config file)
    mSenderPort = 12345;            // Default SuperCollider language port
    mSender.setup(mSenderAddr, mSenderPort);
    ofLog() << "Using server: '" << mSenderAddr << "' on port " << mSenderPort;
    
    // Other OF setup
    mFont.loadFont("Arial.ttf", 14);
    mFontSmall.loadFont("Arial.ttf", 11);
    
	contourFinder.setMinAreaRadius(10);
	contourFinder.setMaxAreaRadius(200);

	trackingColorMode = TRACK_COLOR_RGB;

    tocataImg.allocate(mCaptureWidth, mCaptureHeight, OF_IMAGE_COLOR);
    colorImg.allocate(mCaptureWidth, mCaptureHeight, OF_IMAGE_COLOR);
    
    vShowImage = SHOW_COLOR;
    mStatus = STAT_NONE;

    mFont.loadFont("Arial.ttf", 14);
    mFontSmall.loadFont("Arial.ttf", 11);
    
    threshold = 120;
    targetColor.set(255,255,255);
    
    
}

void testApp::update() {
	mInput.update();
	if(mInput.isFrameNew()) {
        
        colorImg.setFromPixels(mInput.getPixels(), mCaptureWidth, mCaptureHeight, OF_IMAGE_COLOR);
        
        if(mStatus == STAT_READY)
            tocataImg.setTocataImage(mInput.getPixels(), mCaptureWidth, mCaptureHeight, vinylCenter.x,vinylCenter.y, labelRad, vinylRad);
        

        blobInfo.clear();

        // contours que he de mostrar
        contourFinderShow.setTargetColor(targetColor, trackingColorMode);
        contourFinderShow.setThreshold(threshold);
        contourFinderShow.findContours(tocataImg);

        for (size_t j = 0; j < mColorData.size(); j++) {
      
//            threshold = ofMap(mouseX, 0, ofGetWidth(), 0, 255);

            // contour de cada color
            contourFinder.setTargetColor(mColorData[j].color, trackingColorMode);
            contourFinder.setThreshold(mColorData[j].threshold);
            contourFinder.findContours(tocataImg);

            int n = contourFinder.size();
            for(int i = 0; i < n; i++) {

                BlobInfoSt blobInfoTmp;
            
                isActive_blob_info( &(contourFinder.getContour(i)), contourFinder.getBoundingRect(i), &(blobInfoTmp));
                               
                if(blobInfoTmp.isActive)
                {
                    // calc color
                    blobInfoTmp.type = j;
                    blobInfoTmp.color = mColorData[j].color;
                    
                    get_blob_info( &(contourFinder.getContour(i)), contourFinder.getBoundingRect(i), 10, &(blobInfoTmp));
                    
                    
                    
                    blobInfo.push_back(blobInfoTmp);
                }
            }            
        }
        
        // order blobs by y position
        ofSort(blobInfo, sort_fun);
        
        // ENVIO INFO A SC via OSC
        if(mStatus == STAT_READY)
        {
            for (size_t i = 0; i < blobInfo.size(); i++){
                synth_count[blobInfo[i].type]++;
                blobInfo[i].id = synth_count[blobInfo[i].type]+((blobInfo[i].type+1)*1000);
                
                sendOsc(blobInfo[i]);
            }
            
            for(int i=0; i<MAX_TYPES;i++) {
                synth_count_old[i] = synth_count[i];
                synth_count[i] = 0;
            }
        }
        
    }

//	}
}

void testApp::draw() {
	ofSetColor(255);
    string info;

    if(vShowImage == SHOW_CALIBRATE)
    {
        tocataImg.draw(0, 0);
        ofSetLineWidth(2);
        ofNoFill();

        ofSetColor(0,0,200);
        contourFinderShow.draw();
/*
        int n = contourFinderShow.size();
        for(int i = 0; i < n; i++) {
            // smallest rectangle that fits the contour
            ofSetColor(cyanPrint);
            ofPolyline minAreRect = toOf(contourFinder.getMinAreaRect(i));
            minAreRect.draw();
            
            // ellipse that best fits the contour
            ofSetColor(magentaPrint);
            cv::RotatedRect ellipse = contourFinder.getFitEllipse(i);
            ofPushMatrix();
            ofVec2f ellipseCenter = toOf(ellipse.center);
            ofVec2f ellipseSize = toOf(ellipse.size);
            ofTranslate(ellipseCenter.x, ellipseCenter.y);
            ofRotate(ellipse.angle);
            ofEllipse(0, 0, ellipseSize.x, ellipseSize.y);
            ofPopMatrix();
            
            // some different styles of contour centers
            ofVec2f centroid = toOf(contourFinder.getCentroid(i));
            ofSetColor(cyanPrint);
            ofCircle(centroid, 1);
            
            // you can also get the area and perimeter using ofPolyline:
            // ofPolyline::getArea() and ofPolyline::getPerimeter()
            double area = contourFinder.getContourArea(i);
            double length = contourFinder.getArcLength(i);
            
            // balance is useful for detecting when a shape has an "arm" sticking out
            // if balance.length() is small, the shape is more symmetric: like I, O, X...
            // if balance.length() is large, the shape is less symmetric: like L, P, F...
            ofVec2f balance = toOf(contourFinder.getBalance(i));
            ofPushMatrix();
            ofTranslate(centroid.x, centroid.y);
            ofScale(5, 5);
            ofLine(0, 0, balance.x, balance.y);
            ofPopMatrix();
        }
*/
        ofNoFill();
        ofSetColor(75,75,75);
        ofCircle(vinylCenter.x, vinylCenter.y, vinylRad);
        ofCircle(vinylCenter.x, vinylCenter.y, labelRad);

        if (vShowImage == SHOW_CALIBRATE) {
            //
            // Draw the palette of colours, for calibration
            //
            int colorRadius = min(mCaptureWidth / 42, mCaptureHeight / 32);
            int x = mCaptureWidth - colorRadius * 2;
            int y = colorRadius * 2;
            
            for (size_t i = 0; i < mColorData.size(); i++) {
                ofFill();
                y = colorRadius * 2 + ((mColorData.size() - i)) * colorRadius * 2;
                ofSetColor(mColorData[i].color);
                ofCircle(x, y, colorRadius);
                ofSetColor(255,255,255);
                ofNoFill();
                ofRect(x-colorRadius*3-4, y-colorRadius, colorRadius*2-4, colorRadius*2-4);
                ofSetColor(200,200,200);
                ofFill();
                ofRect(x-colorRadius*3-4, y-colorRadius, colorRadius*2-4, colorRadius*2-4);
                drawHighlightString(mColorData[i].mName, x - colorRadius*3 - 66, y-11);
            }
        }
    
        drawHighlightString(ofToString((int) targetColor.r) + " " + ofToString((int) targetColor.g) + " " + ofToString((int) targetColor.b) + " color", 10, 420);
        drawHighlightString(ofToString(threshold) + " threshold", 40, 440);
        ofFill();
        ofSetColor(targetColor);
        ofRect(10, 440, 30, 30);
        
    }else // SHOW_COLOR IMG
    {
        colorImg.draw(0,0);

    }
    
    // SHOW ACTIVE OBJECT INFO
    for (size_t i = 0; i < blobInfo.size(); i++)
    {
        ofSetColor(255,255,255);
        if(blobInfo[i].isActive)
        {
            ofFill();
            ofSetColor(255,0,0);
            ofRect(vinylCenter.x+blobInfo[i].min_pt.x,blobInfo[i].min_pt.y,5, blobInfo[i].max_pt.y-blobInfo[i].min_pt.y);
            
            ofSetColor(255, 255, 255);
            
            int x=7;
            /*
             info = " g:"+ofToString(blobInfo[i].max_pt.y-blobInfo[i].min_pt.y,0);
             ofDrawBitmapString(info, vinylCenter.x+blobInfo[i].min_pt.x+20,blobInfo[i].min_pt.y-x);
             x=x+10;
             info = " a:"+ofToString(blobInfo[i].area);
             ofDrawBitmapString(info, vinylCenter.x+blobInfo[i].min_pt.x+20,blobInfo[i].min_pt.y-x);
             x=x+10;
             info = "ap:"+ofToString(blobInfo[i].area_per,3);
             ofDrawBitmapString(info, vinylCenter.x+blobInfo[i].min_pt.x+20,blobInfo[i].min_pt.y-x);
             x=x+10;
             info = " l:"+ofToString(blobInfo[i].length);
             ofDrawBitmapString(info, vinylCenter.x+blobInfo[i].min_pt.x+20,blobInfo[i].min_pt.y-x);
             x=x+10;
             info = "gm:"+ofToString(blobInfo[i].gr_medi);
             ofDrawBitmapString(info, vinylCenter.x+blobInfo[i].min_pt.x+20,blobInfo[i].min_pt.y-x);
             x=x+10;
             info = " t:"+ofToString(blobInfo[i].tightness,3);
             ofDrawBitmapString(info, vinylCenter.x+blobInfo[i].min_pt.x+20,blobInfo[i].min_pt.y-x);
             x=x+10;
             */
            info = "po:"+ofToString(blobInfo[i].posX,2);
            ofDrawBitmapString(info, vinylCenter.x+blobInfo[i].min_pt.x+20,blobInfo[i].min_pt.y-x);
            x=x+10;
            info = " r:"+ofToString(blobInfo[i].rad,2);
            ofDrawBitmapString(info, vinylCenter.x+blobInfo[i].min_pt.x+20,blobInfo[i].min_pt.y-x);
            x=x+10;
            
            info = " id:"+ofToString(blobInfo[i].id);
            ofDrawBitmapString(info, vinylCenter.x+blobInfo[i].min_pt.x+20,blobInfo[i].min_pt.y-x);
            
            /*                    x=x+10;
             info = "color:"+ofToString((int)blobInfo[i].color.getHue())+","+ofToString((int)blobInfo[i].color.getBrightness())+","+ofToString((int)blobInfo[i].color.getSaturation());
             ofDrawBitmapString(info, vinylCenter.x+blobInfo[i].min_pt.x+20,blobInfo[i].min_pt.y-x);
             */
            x=x+30;
            ofSetColor(blobInfo[i].color.r, blobInfo[i].color.g, blobInfo[i].color.b);
            ofRect(vinylCenter.x+blobInfo[i].min_pt.x+20,blobInfo[i].min_pt.y-x,70,12);
            ofNoFill();
        }
        
    }

    // SHOW STATUS INFO
    switch(mStatus)
    {
        case  STAT_NONE:
            info = "Click on Vinyl Center";
            ofSetColor(80, 30, 50);
            mFont.drawString(info, 250+2, 20+2);
            ofSetColor(240, 100, 150);
            mFont.drawString(info, 250, 20);
            break;
            
        case  STAT_CENTER_CLICKED:
            info = "Click on Sync edge";
            ofSetColor(80, 30, 50);
            mFont.drawString(info, 250+2, 20+2);
            ofSetColor(240, 100, 150);
            mFont.drawString(info, 250, 20);
            break;
            
        case  STAT_LABEL_CLICKED:
            info = "Click on Vinyl edge";
            ofSetColor(80, 30, 50);
            mFont.drawString(info, 250+2, 20+2);
            ofSetColor(240, 100, 150);
            mFont.drawString(info, 250, 20);
            break;
    }

   	
}

void testApp::mousePressed(int x, int y, int button) {

    if (mStatus == STAT_NONE)
    {
        // calc vinyl Center
        vinylCenter.x = mouseX;
		vinylCenter.y = mouseY;
        mStatus = STAT_CENTER_CLICKED;
    }
    else if (mStatus == STAT_CENTER_CLICKED)
    {
        // calc Sync zone / label radius
		labelRad = sqrt((mouseX-vinylCenter.x)*(mouseX-vinylCenter.x) + (mouseY-vinylCenter.y)*(mouseY-vinylCenter.y));
        mStatus = STAT_LABEL_CLICKED;
    }
    else if (mStatus == STAT_LABEL_CLICKED)
    {
        // calc Turntable edge/Radius
        vinylRad = sqrt((mouseX-vinylCenter.x)*(mouseX-vinylCenter.x) + (mouseY-vinylCenter.y)*(mouseY-vinylCenter.y));
        mStatus = STAT_READY;
    }
    else if (vShowImage==SHOW_CALIBRATE) {
        
        int colorRadius = min(ofGetViewportWidth() / 42, ofGetViewportHeight() / 32);
        int x = ofGetViewportWidth() - colorRadius * 2;
        int y;
        
        int dividingX = x - colorRadius;
        bool bAverage = (button != 0);
        float mixAmount = bAverage ? 0.5 : 0;
        if (mixAmount > 0) {
            ofLog() << "Mixing in color with fraction: " << mixAmount;
        }
        // CLICKABLE COLOR CIRCLES
        if (mouseX >= dividingX && mouseY < (mColorData.size()+2)*colorRadius*2) {
            //
            // See if we clicked on the note "palette" to save new colour
            //
            for (size_t i = 0; i < mColorData.size(); i++) {
                y = colorRadius * 2 + ((mColorData.size() - i)) * colorRadius * 2;
                float dist = sqrt(pow(mouseX - x, 2.0) + pow(mouseY - y, 2.0));
                if (dist < colorRadius) {
                    mColorData[i].color = targetColor;
                    mColorData[i].threshold = threshold;
                    saveSettings("wuTocataSetup.txt");
                    break;
                }
            }
        }else if (mouseX >= (dividingX - colorRadius*2)  && mouseY < (mColorData.size()+2)*colorRadius*2)
        {
            //
            // See if we clicked on the note "palette" to select a new colour
            //
            for (size_t i = 0; i < mColorData.size(); i++) {
                y = colorRadius * 2 + ((mColorData.size() - i)) * colorRadius * 2;
                if (mouseY < y+colorRadius && mouseY > y-colorRadius) {
                    targetColor = mColorData[i].color;
                    threshold = mColorData[i].threshold;
                    cout << "touched " << i << endl;
                    break;
                }
            }
        }else
            targetColor = mInput.getPixelsRef().getColor(mouseX, mouseY);

    }


}

void testApp::keyPressed(int key) {

	if(key == 'v')
    {
        vShowImage ++;
        if(vShowImage > NUM_SHOWS-1) vShowImage = 0;
    }

	if(key == 's')
    {
        saveSettings("wuTocataSetup.txt");
    }
   
    // gray threshold
	if(key == OF_KEY_UP) {
		threshold ++;
		if(threshold > 255) threshold = 255;
        cout << threshold << " ";
	}
	if(key == OF_KEY_DOWN) {
		threshold --;
		if(threshold < 0) threshold = 0;
        cout << threshold << " ";
	}
    
    
    if(key == '1')
    {
        targetColor = mColorData[0].color;
        threshold = mColorData[0].threshold;
    }
    if(key == '2')
    {
        if(mColorData.size()>1)
        {
            targetColor = mColorData[1].color;
            threshold = mColorData[1].threshold;
        }
    }
    if(key == '3')
    {
        if(mColorData.size()>2)
        {
            targetColor = mColorData[2].color;
            threshold = mColorData[2].threshold;
        }
    }
    if(key == '4')
    {
        if(mColorData.size()>3)
        {
            targetColor = mColorData[3].color;
            threshold = mColorData[3].threshold;
        }
    }
    if(key == '5')
    {
        if(mColorData.size()>4)
        {
            targetColor = mColorData[4].color;
            threshold = mColorData[4].threshold;
        }
    }
    if(key == '6')
    {
        if(mColorData.size()>5)
        {
            targetColor = mColorData[5].color;
            threshold = mColorData[5].threshold;
        }
    }
    if(key == '7')
    {
        if(mColorData.size()>6)
        {
            targetColor = mColorData[6].color;
            threshold = mColorData[6].threshold;
        }
    }
    if(key == '8')
    {
        if(mColorData.size()>7)
        {
            targetColor = mColorData[7].color;
            threshold = mColorData[7].threshold;
        }
    }
    if(key == '9')
    {
        if(mColorData.size()>8)
        {
            targetColor = mColorData[8].color;
            threshold = mColorData[8].threshold;
        }
    }
    if(key == '0')
    {
        if(mColorData.size()>9)
        {
            targetColor = mColorData[9].color;
            threshold = mColorData[9].threshold;
        }
    }
    
}

void testApp::isActive_blob_info(vector<cv::Point> * blob, cv::Rect boundingRect, BlobInfoSt * info)
{
    
    int offset = 0;   //default =15
    int maxOffset = 25;
    info->min_pt.y = 2000;
    info->max_pt.y = 0;
    info->isActive = false;
    bool ptdetectmax = false;
    bool ptdetectmin = false;
    bool amplesuf = false;
    
    // is the blob crossing the scan line?
    do{
        offset=offset+1;
        amplesuf = false;
        
        for(int i=0; i< blob->size(); i++) {
            if( (*blob)[i].y > info->max_pt.y && (*blob)[i].x < vinylCenter.x+offset && (*blob)[i].x > vinylCenter.x-offset && boundingRect.x < vinylCenter.x && (boundingRect.x+boundingRect.width)> vinylCenter.x)
            {
                info->max_pt.y = (*blob)[i].y;
                ptdetectmax = true;
            }
            if((*blob)[i].y < info->min_pt.y && (*blob)[i].x < vinylCenter.x+offset && (*blob)[i].x > vinylCenter.x-offset && boundingRect.x < vinylCenter.x && (boundingRect.x+boundingRect.width)> vinylCenter.x)
            {
                info->min_pt.y = (*blob)[i].y;
                ptdetectmin = true;
            }
        }
        
 //       if (ptdetectmax && ptdetectmin && (info->max_pt.y - info->min_pt.y)>6)
 //           amplesuf = true ;
        
    }while(!amplesuf && offset<maxOffset);
    
    if(ptdetectmax && ptdetectmin && (info->max_pt.y - info->min_pt.y)>0)
    {
        info->isActive = true;
    }
    
}


void testApp::get_blob_info(vector<cv::Point> * blob, cv::Rect boundingRect, int area, BlobInfoSt * info)
{
    
    bool ptdetectmax = false;
    bool ptdetectmin = false;
    
    // detecta el gruix mitj�
    int count = 0;
    info->gr_medi = 0;
    int off_med = 1;
    
    for(int j=boundingRect.x; j < boundingRect.x + boundingRect.width; j++) {
        
        int max_pt=0;
        int min_pt=2000;
        
        for(int i=0; i< blob->size(); i++) {
            if((*blob)[i].y > max_pt && (*blob)[i].x == j)
                //						if(blob->pts[i].y > max_pt && blob->pts[i].x < j+off_med && blob->pts[i].x > j-off_med)
            {
                max_pt = (*blob)[i].y;
                ptdetectmax = true;
            }
            if((*blob)[i].y < min_pt && (*blob)[i].x == j)
            {
                min_pt = (*blob)[i].y;
                ptdetectmin = true;
            }
        }
        if(ptdetectmax && ptdetectmin && max_pt-min_pt>4)
        {
            info->gr_medi = info->gr_medi + (max_pt-min_pt);
            count++;
        }
    }
    
    if( count>0)
        info->gr_medi = info->gr_medi/count;
    else
        info->gr_medi = 0;
    
    // altres valors
    info->tightness = boundingRect.width/boundingRect.height;
    info->area = area;
    info->area_per = (boundingRect.width*boundingRect.height)/area;
    //				info->area_tight = info->area_per * info->tightness * 1000/info->area;
    info->gruix = info->max_pt.y-info->min_pt.y;
    info->posX = 1.-((vinylCenter.x-boundingRect.x)/boundingRect.width);
    info->rad = 1.-((info->max_pt.y - (vinylCenter.y+labelRad))/ (vinylRad-labelRad));
    
}


void testApp::sendOsc(BlobInfoSt &info){
    ofxOscMessage m;
    m.setAddress("/tocata");
    m.addIntArg((int)(info.id/1000));
    m.addIntArg(info.id);
    //    m.addFloatArg( ofMap(info.min_pt.y,0,ofGetHeight(), 0.0,1.0));
    m.addFloatArg( info.rad);
    m.addFloatArg( ofMap(info.gruix,0,ofGetHeight()/2, 0.0,1.0));
    m.addIntArg(info.gr_medi);
    m.addFloatArg( info.tightness);
    m.addIntArg(info.area);
    m.addFloatArg( info.area_per);
    m.addFloatArg( info.posX);
    
    mSender.sendMessage(m);
    
    
}

