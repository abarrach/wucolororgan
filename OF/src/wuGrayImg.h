
#ifndef wu_GRAYIMG_H
#define wu_GRAYIMG_H

#include "ofxOpenCv.h"

class wuGrayImg : public ofxCvGrayscaleImage {
public:
	wuGrayImg();
	~wuGrayImg();
    
	void isolateColor(ofxCvColorImage *videoColorCvImage, int hue, int sat, int val, int hueRange, int satRange, int valRange, bool invert=false);
	void mix(ofxCvGrayscaleImage *grayInput, int threshold=100, bool invert=false);
	void set_antiTocata(int centreX, int centreY, int inner_rad, int extern_rad, int vw, int vh, bool invert=false);
	void setFromAlphaPixels(unsigned char * colorPixels, int vw, int vh, bool isTexture=false);
	void deleteWhite(ofxCvColorImage *videoColorCvImage);

private:

};

#endif
